/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import colecciones.ListaCD;
import java.util.Iterator;
/**
 *
 * @author estudiante
 */
public class TestListaCD {
    
    
    public static void main(String[] args) {
        
     ListaCD<String> nombres=new ListaCD();
     nombres.insertarInicio("peña");
     nombres.insertarInicio("boris");
     nombres.insertarInicio("alejandro");
     nombres.insertarFinal("leydi");
     System.out.println(nombres);
     
     for(String dato:nombres)
            System.out.println(dato);
     
     
     //forma 2 de usar iterador:
     Iterator<String> it=nombres.iterator();
     while(it.hasNext())
            System.out.println(it.next().toString());
     
     
     
     ListaCD<Integer> numeros=new ListaCD();
     int n=1000000;
     
     long tiempo=System.currentTimeMillis();
     for(int i=0;i<n;i++)
        numeros.insertarFinal(i);
     
     tiempo=System.currentTimeMillis()-tiempo;
     tiempo=tiempo/1000;
        System.out.println("Se demoro en insertando:"+tiempo+"segundos");
     //De la manera menos menos menos óptima:
     //Estamos simulando un vector   
     tiempo=System.currentTimeMillis();
     //variable mentirosa:
     long c=0;
//      for(int i=0;i<numeros.getTamanio();i++)  
//            //System.out.println(numeros.get(i));
//            c+=numeros.get(i);
        
        for(int dato:numeros)
              c+=dato;

      tiempo=System.currentTimeMillis()-tiempo;
     tiempo=tiempo/1000;
        System.out.println("Se demoro en iterator:"+tiempo+"segundos");
        
        
        //Pruebas de Concatenación y Borrado.
        System.out.println("\n\n\n\n\n");
        ListaCD <String> listaCD2 = new ListaCD() ;
        listaCD2.insertarFinal("Hernesto");
        listaCD2.insertarFinal("Miguel");
        listaCD2.insertarFinal("Hernesto");
        listaCD2.insertarFinal("Miguel");
        listaCD2.insertarFinal("Daniela");
        listaCD2.insertarFinal("Arley");
        listaCD2.insertarFinal("Jhonnathan");
        listaCD2.insertarFinal("Hernesto");
        listaCD2.insertarFinal("Miguel");
        listaCD2.insertarFinal("Hernesto");
        listaCD2.insertarFinal("Miguel");
        listaCD2.insertarFinal("Hernesto");
        listaCD2.insertarFinal("Miguel");
        
        
        
        System.out.println("Lista Número 1: "+nombres+"\nTamaño Lista Número 1: "+nombres.getTamanio());
        System.out.println("Lista Numero 2: "+listaCD2+"\nTamaño Lista Número 2: "+listaCD2.getTamanio());
        
        nombres.concatenar(listaCD2);
        
        
        System.out.println("Lista Número 1: "+nombres+"\nTamaño Lista Número 1: "+nombres.getTamanio());
        System.out.println("Lista Numero 2: "+listaCD2+"\nTamaño Lista Número 2: "+listaCD2.getTamanio());
        
        
        //Prueba De Eliminar Repetidos Dentro De La Lista
        
        System.out.println("\n\n\n\n\nLuego De Eliminar Repetidos...\n\n");
       
        nombres.eliminarRepetidos();
        listaCD2.eliminarRepetidos();
        System.out.println("Lista Número 1: "+nombres+"\nTamaño Lista Número 1: "+nombres.getTamanio());
        System.out.println("Lista Numero 2: "+listaCD2+"\nTamaño Lista Número 2: "+listaCD2.getTamanio());
        
        ListaCD <Integer> numeros0 = new ListaCD();
        
        ListaCD <Integer> numeros1 = new ListaCD();
        
        
        numeros0.insertarFinal(1);
        numeros0.insertarFinal(2);
        numeros0.insertarFinal(3);
        numeros0.insertarFinal(4);
        numeros0.insertarFinal(5);
        numeros0.insertarFinal(5);
        
        numeros1.insertarFinal(3);
        numeros1.insertarFinal(2);
        numeros1.insertarFinal(3);
        numeros1.insertarFinal(3);
        numeros1.insertarFinal(4); 
        System.out.println("\n\n\n\n\nSin Eliminar Repetidos");
        System.out.println("Lista Número 1: "+numeros0+"\nTamaño Lista Número 1: "+numeros0.getTamanio());
        System.out.println("Lista Numero 2: "+numeros1+"\nTamaño Lista Número 2: "+numeros1.getTamanio());
        System.out.println("\n\n\n\n\n");
        numeros0.eliminarRepetidos();
        System.out.println("Luego de eliminados los repetidos...\n\nLista Número 1: "+numeros0+"\nTamaño Lista Número 1: "+numeros0.getTamanio());
        numeros1.eliminarRepetidos();
        System.out.println("Lista Numero 2: "+numeros1+"\nTamaño Lista Número 2: "+numeros1.getTamanio());
        
        
        //Pruebas De Borrar Incidencias En Listas Circulares
        System.out.println("\n\n\n\n\n");
        System.out.println("\n\n\n\n\n");
        System.out.println("\n\n\n\n\n");
        
        
        ListaCD <Integer> list = new ListaCD();
        list.insertarFinal(1);
        list.insertarFinal(2);
        list.insertarFinal(5);
        list.insertarFinal(13);
        list.insertarFinal(1);
        list.insertarFinal(2);
        list.insertarFinal(4);
        
        ListaCD <Integer> list1 = new ListaCD();
        list1.insertarFinal(1);
        list1.insertarFinal(2);
        System.out.println("\n\n\n\n\nDatos Sin Borrar Incidencias...");
        
        System.out.println("Lista Número 1: "+list+"\nTamaño De Lista Número 1: "+list.getTamanio());
        System.out.println("Lista Número 2: "+list1+"\nTamaño De Lista Número 2: "+list1.getTamanio());
        
        System.out.println("\n\n\n\n\nDatos Luego de Borradas Las Incidencias...");
        
       list.eliminarIncidencias(list1);
        System.out.println("Lista Número 1: "+list+"\nTamaño De Lista 1: "+list.getTamanio());
        
    }
}
