/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.Iterator;

/**
 *
 * @author estudiante
 * @param <T>
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {

        this.cabeza = new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);

    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza.getSig(), this.cabeza);
        //redireccionar
        this.cabeza.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);

        this.tamanio++;
    }

    //O(1)
    public void insertarFinal(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo();
            if (x != this.cabeza.getAnt()) {
                msg += ", ";
            } else {
                msg += ".";
            }
        }
        return msg;
    }

    public boolean esVacio() {

        return (this.cabeza == this.cabeza.getSig());
        //return (this.cabeza==this.cabeza.getAnt());
        //return this.tamanio==0;
    }

    public int getTamanio() {
        return tamanio;
    }

    public T get(int i) {
        try {
            return this.getPos(i).getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }

    public void set(int i, T info) {
        try {
            this.getPos(i).setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    private NodoD<T> getPos(int i) throws Exception {
        if (this.esVacio() || i < 0 || i >= this.tamanio) {
            throw new Exception("El índice esta fuera de rango de la lista circular doble");
        }

        NodoD<T> x = this.cabeza.getSig();

        while (i > 0) {
            x = x.getSig();
            i--;
        }
        return x;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorListaCD(this.cabeza);
    }

    public void erase() {
        if (!this.esVacio()) {
            this.cabeza.setAnt(this.cabeza);
            this.cabeza.setSig(this.cabeza);
            this.tamanio = 0;
        }

    }

    public void concatenar(ListaCD<T> l2) {
        if (!l2.esVacio()) {
            this.cabeza.getAnt().setSig(l2.cabeza.getSig());
            this.cabeza.setAnt(l2.cabeza.getAnt());
            this.cabeza.getAnt().setSig(this.cabeza);
            this.tamanio += l2.getTamanio();
            l2.erase();
        }
    }

    public void eliminarRepetidos() {
        if (!this.esVacio()) {
            for (NodoD i = this.cabeza.getSig(); i != this.cabeza.getAnt(); i = i.getSig()) {

                for (NodoD j = i.getSig(); j != this.cabeza; j = j.getSig()) {

                    if (i.getInfo() == j.getInfo()) {
                        j.getSig().setAnt(j.getAnt());
                        j.getAnt().setSig(j.getSig());
                        this.tamanio--;
                    }
                }
            }
        }
    }

    public void eliminarIncidencias(ListaCD<T> l2) {
        boolean comprobacion = true;
        for (NodoD i = this.cabeza.getSig(); i != this.cabeza; i = i.getSig()) {
            if (i.getInfo() == l2.cabeza.getSig().getInfo()) {
                 NodoD h = i;
               for (NodoD j = l2.cabeza.getSig(); j != l2.cabeza; j = j.getSig()) {
                       
                   if (h.getInfo().equals(j.getInfo())&&comprobacion) {
                      h = h.getSig();
                    }else comprobacion = false;

                }
                if (comprobacion) {
                    NodoD aux = i;
                    for (int j = 0; j < l2.getTamanio(); j++) {
                        aux = aux.getSig();
                    }

                    i.getAnt().setSig(aux);
                    aux.setAnt(i.getAnt());
                    this.tamanio-=l2.getTamanio();

                }
                comprobacion = true;
                
            }
            
        }

    }

}
